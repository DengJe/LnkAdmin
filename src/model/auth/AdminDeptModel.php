<?php

declare(strict_types=1);

namespace LnkAdmin\model\auth;

use LnkAdmin\model\BaseModel;

class AdminDeptModel extends BaseModel
{
    protected $name = 'admin_dept';
    protected $autoWriteTimestamp = false;


    /**
     * 更新
     */
    public static function updateRel($adminId,array $deptIds)
    {
        // 删除
        self::where('admin_id',$adminId)->delete();
        $data = [];
        foreach($deptIds as $v){
            $data[] = [
                'admin_id' => $adminId,
                'dept_id' => $v
            ];
        }
        // 插入
        self::insertAll($data);
    }
}