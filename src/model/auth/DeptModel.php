<?php

declare(strict_types=1);

namespace LnkAdmin\model\auth;

use LnkAdmin\model\BaseModel;

class DeptModel extends BaseModel
{
    protected $name = 'dept';
    protected $autoWriteTimestamp = true;
}