<?php

declare(strict_types=1);

namespace LnkAdmin\model\auth;

use LnkAdmin\model\BaseModel;

class AdminRoleModel extends BaseModel
{
    protected $name = 'admin_role';
    protected $autoWriteTimestamp = false;

    /**
     * 更新
     */
    public static function updateRel($adminId,array $roleIds)
    {
        // 删除
        self::where('admin_id',$adminId)->delete();
        $data = [];
        foreach($roleIds as $v){
            $data[] = [
                'admin_id' => $adminId,
                'role_id' => $v
            ];
        }
        // 插入
        self::insertAll($data);
    }
}
