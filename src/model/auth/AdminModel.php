<?php

declare(strict_types=1);

namespace LnkAdmin\model\auth;

use LnkAdmin\model\BaseModel;
use LnkAdmin\service\FileService;

class AdminModel extends BaseModel
{
    protected $name = 'admin';

    protected $type = [
        'login_time'  =>  'timestamp',
    ];

    protected $append = ['dept_ids','role_ids'];

    protected $json = [];    // 自动 json 转换


    public function getAvatarAttr($value)
    {
        return FileService::getFileUrl($value);
        
    }
    
    public function getRoleIdsAttr()
    {
        return AdminRoleModel::where('admin_id',$this->id)->column('role_id');
    }

    public function getDeptIdsAttr()
    {
        return AdminDeptModel::where('admin_id',$this->id)->column('dept_id');
    }
    

    public function role()
    {
        return $this->hasOne(RoleModel::class, "id", "role_id")->field("id, name");
    }
}
