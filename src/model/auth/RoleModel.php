<?php

declare(strict_types=1);

namespace LnkAdmin\model\auth;

use LnkAdmin\model\BaseModel;

class RoleModel extends BaseModel
{
    protected $name = 'role';
    protected $autoWriteTimestamp = true;
}