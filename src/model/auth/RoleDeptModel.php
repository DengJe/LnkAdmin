<?php

declare(strict_types=1);

namespace LnkAdmin\model\auth;

use LnkAdmin\model\BaseModel;

class RoleDeptModel extends BaseModel
{
    protected $name = 'role_dept';
    protected $autoWriteTimestamp = false;


    /**
     * 更新
     */
    public static function updateRel($roleId,array $deptIds)
    {
        // 删除
        self::where('role_id',$roleId)->delete();
        $data = [];
        foreach($deptIds as $v){
            $data[] = [
                'role_id' => $roleId,
                'dept_id' => $v
            ];
        }
        // 插入
        self::insertAll($data);
    }
}