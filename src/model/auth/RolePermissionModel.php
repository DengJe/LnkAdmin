<?php

declare(strict_types=1);

namespace LnkAdmin\model\auth;

use LnkAdmin\model\BaseModel;

class RolePermissionModel extends BaseModel
{
    protected $name = 'role_permission';
    protected $autoWriteTimestamp = false;


    /**
     * 更新
     */
    public static function updateRel($roleId,array $rules)
    {
        // 删除
        self::where('role_id',$roleId)->delete();
        $data = [];
        foreach($rules as $v){
            $data[] = [
                'role_id' => $roleId,
                'rule' => $v
            ];
        }
        // 插入
        self::insertAll($data);
    }
}