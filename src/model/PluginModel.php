<?php

declare(strict_types=1);

namespace LnkAdmin\model;

use LnkAdmin\model\BaseModel;

class PluginModel extends BaseModel
{
    protected $name = 'plugin';
}