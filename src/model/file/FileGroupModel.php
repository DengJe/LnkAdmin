<?php

declare(strict_types=1);

namespace LnkAdmin\model\file;

use LnkAdmin\model\BaseModel;

class FileGroupModel extends BaseModel
{
    protected $name = 'file_group';

    // 自动数据类型转换
    protected $type = [];

    // 自动 json 转换
    protected $json = [];
}