<?php

declare(strict_types=1);

namespace LnkAdmin\model\dict;

use LnkAdmin\model\BaseModel;

class DictTypeModel extends BaseModel
{
    protected $name = 'dict_type';

    // protected $json = ['column_list'];

    public function getColumnListAttr($v)
    {
        if($v){
            return json_decode($v);
        }
        return [];
    }

    public function setColumnListAttr($v)
    {
        
        return json_encode($v);
        
    }
}