<?php

declare(strict_types=1);

namespace LnkAdmin\model\dict;

use LnkAdmin\model\BaseModel;

class DictDataModel extends BaseModel
{
    protected $name = 'dict_data';


    public function setValueAttr($v)
    {
        return json_encode($v);
    }


    public function getValueAttr($v)
    {
        return json_decode($v);
    }
}