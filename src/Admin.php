<?php

namespace LnkAdmin;


use think\Service;
use LnkAdmin\command\InstallCommand;
use LnkAdmin\command\PluginCreateCommand;
use LnkAdmin\command\AdminBuildCommand;
use LnkAdmin\library\plugin\Plugin;
use LnkAdmin\middleware\Cors;
use LnkAdmin\model\auth\Permission;
use LnkAdmin\service\MenuService;
use think\App;
use think\facade\Middleware;


class Admin
{
    public static $root = 'app';

    public const version = '1.0.0';

    

    public static function directory(): string
    {
        return app()->getRootPath() . self::$root . DIRECTORY_SEPARATOR;
    }

    public function loadAdminRoute()
    {
        require(__DIR__.DIRECTORY_SEPARATOR.'route'.DIRECTORY_SEPARATOR.'admin.php');
    }


    /**
     * 获取全部插件目录
     */
    public static function getPluginsDirectory():array
    {
        $plugis = [];
        foreach(scandir(self::directory()) as $dir){
            if($dir == '.' || $dir == '..'){
                continue;
            }
            $plugis[] = root_path('app/'.$dir);  
        }
        return $plugis;
    }


    /**
    * 获取插件服务类
    */
    public static function getServices()
    {
        $services= [];
        foreach(scandir(self::directory()) as $dir){
            if($dir == '.' || $dir == '..'){
                continue;
            }
            if(is_file(self::directory().$dir.DIRECTORY_SEPARATOR."ServiceProvider.php")){
                $services[] = "\app\\{$dir}\ServiceProvider";
            }
            
        }
      
        return $services;
    }


    /**
     * 构建后台菜单
     */
    public static function buildAdminMenu()
    {
        $menuFile = __DIR__.'/menu.php';
        $menuTree = require($menuFile);
        $menu = MenuService::buildMenuList($menuTree);
        Permission::where('plugin','admin')->delete();
        foreach($menu as $v){
            $v['plugin'] = 'admin';
            Permission::create($v);
        }
    }


    /**
     * 发布后台静态文件
     */
    public static function publishAdminStatic()
    {

    }


    


   



}