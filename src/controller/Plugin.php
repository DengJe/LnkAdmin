<?php

declare(strict_types=1);

namespace LnkAdmin\controller;

use LnkAdmin\controller\Backend;
use LnkAdmin\model\PluginModel;
use LnkAdmin\plugin\Manage;
use think\Request;

class Plugin extends Backend
{
  /**
   * 应用列表
   */
  public function list()
  {
    $plugins = Manage::plugins();
    $list = [];
    foreach ($plugins as $k => $v) {

      $list[] =  Manage::plugin($v);
      $installedPlugin = PluginModel::where('plugin', $v)->find();
      $list[$k]['install'] = false;
      if ($list[$k]['plugin'] == @$installedPlugin->plugin) {
        $list[$k]['status'] = $installedPlugin['status'];
        $list[$k]['install'] = true;
      } else {
        $list[$k]['status'] = 'disable';
      }
    }
    return success(compact('list'));
  }


  /**
   * 安装
   */
  public function install(Request $request)
  {
    $plugin = $request->post('plugin');
    Manage::install($plugin);
    return success("安装成功");
  }

  /**
   * 启用
   */
  public function enable(Request $request)
  {
    $plugin = $request->post('plugin');
    Manage::enable($plugin);
    return success("启用成功");
  }


  /**
   * 禁用
   */
  public function disable(Request $request)
  {
    $plugin = $request->post('plugin');
    Manage::disable($plugin);
    return success("禁用成功");
  }
}
