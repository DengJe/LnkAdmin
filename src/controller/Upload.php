<?php
declare(strict_types=1);

namespace LnkAdmin\controller;


use LnkAdmin\controller\Backend;
use LnkAdmin\service\UploadService;
use think\Request;

class Upload extends Backend
{
    /**
     * 上传图片
     */
    public function image(Request $request)
    {
        $file = UploadService::image($request->post('group_id',0));
        return success('上传成功',$file);
    }

    /**
     * 上传证书
     */
    public function uploadCert(Request $request)
    {
        $file = UploadService::cert();
        return success('上传成功',$file);
    }
}