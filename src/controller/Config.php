<?php

declare(strict_types=1);

namespace LnkAdmin\controller;

use LnkAdmin\controller\Backend;
use LnkAdmin\model\ConfigModel;
use LnkAdmin\controller\traits\Crud;
use LnkAdmin\service\ConfigService;
use LnkAdmin\service\FileService;
use think\facade\Db;

class Config extends Backend
{
    use Crud;

    public function initialize()
    {
        $this->model = new ConfigModel();
    }


    /**
     * 后台初始化配置
     */
    public function initConfig()
    {
       
        $data = [
            // 文件域名
            'oss_domain' => FileService::getFileUrl(),
            // 网站名称
            'web_name' => ConfigService::get('website', 'name'),
            // 网站图标
            'web_favicon' => FileService::getFileUrl(ConfigService::get('website', 'web_favicon','')),
            // 网站logo
            'web_logo' => FileService::getFileUrl(ConfigService::get('website', 'web_logo','')),
            // 登录页
            'login_image' => FileService::getFileUrl(ConfigService::get('website', 'login_image','')),
            // 版权信息
            'copyright_config' => ConfigService::get('copyright', 'config', []),
            // 控制台视图
            'dashboard_component' => ConfigService::get('website','dashboard_component','admin/views/dashboard/index'),
             // JWT
             'jwt' => [
                //JWT time to live
                'ttl'         => config('jwt.ttl',3600),
                //Refresh time to live
                'refresh_ttl' => config('jwt.refresh_ttl'),
            ],
        ];
        return success($data);
    }


   

   

  
}
