<?php

declare(strict_types=1);

namespace LnkAdmin\controller;


use think\Request;
use app\BaseController;
use LnkAdmin\Admin;
use LnkAdmin\controller\Backend;
use LnkAdmin\service\InstallService;
use think\facade\Cache;
use think\Template;

const SUCCESS = 'success';
const ERROR = 'error';

class Install extends Backend
{
    private $template;
    function __construct()
    {
        if(is_file(config_path().'install.lock')){
            exit('已经安装不能重复安装');
        }
        $this->template = (new Template())->config(['view_path'=>__DIR__.'/../view/','cache_path'=>runtime_path('cache').date('d').'/']);
    }
    public function index()
    {
        return $this->template->fetch('/install/index');
    }

    public function step1()
    {
        if (request()->isPost()) {

            // 检测生产环境
            foreach ($this->checkEnv() as $key => $value) {
                if ($key == 'php' && (float)$value < 8.0) {
                    return error('PHP版本过低！');
                }
            }

            // 检测目录权限
            foreach ($this->checkDirFile() as $value) {
                if ($value[1] == ERROR
                    || $value[2] == ERROR) {
                    return error($value[3] . ' 权限读写错误！');
                }
            }

            Cache::set('checkEnv', 'success');
            return json(['code' => 200, 'url' => '/install/step2']);
        }
        return $this->template->fetch('/install/step1',[
            'checkEnv' => $this->checkEnv(),
            'checkDirFile' => $this->checkDirFile(),
        ]);
    }

     /**
     * 检测环境变量
     * @return array
     */
    protected function checkEnv(): array
    {
        $items['php'] = PHP_VERSION;
        $items['mysqli'] = extension_loaded('mysqli');
        $items['redis'] = extension_loaded('redis');
        $items['curl'] = extension_loaded('curl');
        $items['fileinfo'] = extension_loaded('fileinfo');
        $items['exif'] = extension_loaded('exif');
        $items['swoole_loader'] = extension_loaded('swoole_loader');
        return $items;
    }

    /**
     * 检测读写环境
     * @return array
     */
    protected function checkDirFile(): array
    {
        $items = array(
            array('dir', SUCCESS, SUCCESS, './'),
            array('dir', SUCCESS, SUCCESS, './public'),
            array('dir', SUCCESS, SUCCESS, './runtime'),
            // array('dir', SUCCESS, SUCCESS, './public/uploads'),
           
        );

        foreach ($items as &$value) {

            $item = root_path() . $value[3];

            // 写入权限
            if (!is_writable($item)) {
                $value[1] = ERROR;
            }

            // 读取权限
            if (!is_readable($item)) {
                $value[2] = ERROR;
            }
        }

        return $items;
    }


    /**
     * 检查环境变量
     *
     * 
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function step2()
    {

        // if (!Cache::get('checkEnv')) {
        //     return redirect('/install/step1');
        // }

        if (request()->isPost()) {

            // 链接数据库
            $params = request()->all();
            $connect = @mysqli_connect($params['hostname'] . ':' . $params['hostport'], $params['username'], $params['password']);
            if (!$connect) {
                return error('数据库链接失败');
            }

            // 检测MySQL版本
            $mysqlInfo = @mysqli_get_server_info($connect);

            // 查询数据库名
            $database = false;
            $mysql_table = @mysqli_query($connect, 'SHOW DATABASES');
            while ($row = @mysqli_fetch_assoc($mysql_table)) {
                if ($row['Database'] == $params['database']) {
                    $database = true;
                    break;
                }
            }

            if (!$database) {
                $query = "CREATE DATABASE IF NOT EXISTS `" . $params['database'] . "` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;";
                if (!@mysqli_query($connect, $query)) {
                    return error('数据库创建失败或已存在，请手动修改');
                }
            }

            Cache::set('mysqlInfo', $params);
            return json(['code' => 200, 'url' => '/install/step3']);
        }

        return $this->template->fetch('/install/step2');
    }

    /**
     * 初始化数据库
     * @return \support\Response
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function step3()
    {
        $mysqlInfo = Cache::get('mysqlInfo');
        if (!$mysqlInfo) {
            return redirect('/install/step2');
        }

        return $this->template->fetch('/install/step3',[]);
    }

    /**
     * 安装数据缓存
     * @return \support\Response|void
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function install()
    {
        if (request()->isAjax()) {

            $mysqlInfo = Cache::get('mysqlInfo');
            if (is_file(config_path('install.lock')) || !$mysqlInfo) {
                return error('请勿重复安装本系统');
            }

            InstallService::execSql($mysqlInfo);
            InstallService::putEnv($mysqlInfo);
            
            
            $sqlConnect = @mysqli_connect($mysqlInfo['hostname'] . ':' . $mysqlInfo['hostport'], $mysqlInfo['username'], $mysqlInfo['password']);
            mysqli_select_db($sqlConnect, $mysqlInfo['database']);
            mysqli_query($sqlConnect, "set names utf8mb4");
          
            $pwd = $mysqlInfo['pwd'];
            $salt = random_int(1000,9999);
            $pwd =encrypt_password($pwd,$salt);
            mysqli_query($sqlConnect, "UPDATE {$mysqlInfo['prefix']}admin SET  password='{$pwd}' , salt='{$salt}' where is_super = 1");

            return success('success',['admin_url'=>get_domain().'/super']);
        }else{
            return redirect('/install/index');
        }
    }

   

    /**
     * 清理安装文件包
     *
     * @return \support\Response|void
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function clear()
    {

        if (request()->isAjax()) {
            try {
           
                
                $mysqlInfo = Cache::get('mysqlInfo');
                
              
                Admin::buildAdminMenu();
                file_put_contents(config_path().'install.lock','');

                // 清理安装包
                Cache::clear();
              
            } catch (\Throwable $th) {
                return error($th->getMessage());
            }

            return success('安装成功');
        }
    }
}