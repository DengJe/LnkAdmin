<?php

declare(strict_types=1);

namespace LnkAdmin\controller;

use LnkAdmin\exception\LiteException;
use LnkAdmin\facade\Auth;

class Api
{
    /**
     * 当前用户 auth 实例
     *
     * @return Auth
     */
    public function auth()
    {
        return Auth::guard('user');
    }
}
