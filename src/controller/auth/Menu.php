<?php

declare(strict_types=1);

namespace app\admin\controller\auth;

use LnkAdmin\controller\Backend;
use LnkAdmin\model\auth\AdminModel;
use LnkAdmin\service\MenuService;
use LnkAdmin\controller\traits\Crud;

class Menu extends Backend
{
    use Crud;
    

    public function tree()
    {
        return success(MenuService::tree());
    }
}