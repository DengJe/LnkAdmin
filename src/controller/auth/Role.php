<?php

declare(strict_types=1);

namespace LnkAdmin\controller\auth;

use LnkAdmin\controller\Backend;
use LnkAdmin\model\auth\AdminModel;
use LnkAdmin\model\auth\RoleModel;
use app\Request;
use LnkAdmin\controller\traits\Crud;
use LnkAdmin\model\auth\RoleDeptModel;
use LnkAdmin\model\auth\RolePermissionModel;
use think\facade\Db;

class Role extends Backend
{
    use Crud;
    protected $childAdminIds = [];

    public function initialize()
    {
        $this->model = new RoleModel();
    }

    /**
     * 详情
     *
     * @param  $id
     * @return \think\Response
     */
    public function read($id)
    {
        $detail = $this->model->findOrFail($id);
        $detail->menu_permission = RolePermissionModel::where('role_id',$id)->column('rule');
        $detail->data_permission = RoleDeptModel::where('role_id',$id)->column('dept_id');
        return success('获取成功', $detail);
    }


    public function tree()
    {
        $list = $this->model->select();   
        return success($list);
    }

    /**
     * 更新角色菜单权限
     */
    public function updateMenuPermission(Request $request)
    {
        $roleId = $request->put('id');
        $rules = $request->put('rules');
        RolePermissionModel::updateRel($roleId,$rules);
        return success('更新成功');
    }

    /**
     * 更新角色数据权限
     */
    public function updateDataPermission(Request $request)
    {
        $roleId = $request->post('id');
        $dataScope = $request->post('data_scope');
        $deptIds = $request->post('dept_ids',[]);
        $this->model->where('id',$roleId)->update(['data_scope'=>$dataScope]);
        RoleDeptModel::updateRel($roleId,$deptIds);
        return success('更新成功');
    }

  
}
