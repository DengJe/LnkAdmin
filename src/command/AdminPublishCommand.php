<?php
namespace LnkAdmin\command;

use app\admin\model\auth\AdminModel;
use LnkAdmin\Admin;
use LnkAdmin\plugin\Manage;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use LnkAdmin\service\AppService;


class AdminPublishCommand extends Command
{
    protected function configure()
    {
        $this->setName('admin:publish')
        	->setDescription('发布LnkAdmin静态文件');
    }

    protected function execute(Input $input, Output $output)
    {
        // 发布admin-view
        // 备份
        copy_directory(__DIR__.DIRECTORY_SEPARATOR.'../../admin-view/src/lnk',runtime_path('backups/admin/admin-view/'.time().'/src/lnk').DIRECTORY_SEPARATOR);
        // 删除
        delete_directory(root_path('admin-view/src/lnk'));
        // 复制
        copy_directory(__DIR__.DIRECTORY_SEPARATOR.'../../admin-view/',root_path('admin-view/'));
        // 创建app目录
        if(!is_dir(root_path().'admin-view/src/app/')){
            mkdir(root_path().'admin-view/src/app/');
        }
        // 发布admin-static
        copy_directory(__DIR__.DIRECTORY_SEPARATOR.'../../admin-static',public_path('admin-static'));
        // 构建菜单
        Admin::buildAdminMenu();
    	$output->info('LnkAdmin 发布成功');
    }
}