<?php
namespace LnkAdmin\command;

use app\admin\model\auth\AdminModel;
use LnkAdmin\Admin;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use LnkAdmin\service\AppService;
use LnkAdmin\support\SqlExecute;

class AdminInstallCommand extends Command
{
    protected function configure()
    {
        $this->setName('admin:install')
        	->setDescription('安装LnkAdmin核心包');
    }

    protected function execute(Input $input, Output $output)
    {
        Admin::buildAdminMenu();
        (new SqlExecute())->execInstallSql('admin');

    	$output->info('LnkAdmin 安装成功！');
    }
}