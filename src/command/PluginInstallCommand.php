<?php
namespace LnkAdmin\command;

use app\admin\model\auth\AdminModel;
use LnkAdmin\plugin\Manage;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use LnkAdmin\service\AppService;


class PluginInstallCommand extends Command
{
    protected function configure()
    {
        $this->setName('plugin:install')
            ->addArgument('plugin', Argument::OPTIONAL, 'plugin name .')
        	->setDescription('安装插件');
    }

    protected function execute(Input $input, Output $output)
    {
        $plugin = $input->getArgument('plugin');
        if(!$plugin){
            $output->error('请输入插件名称');
        }
        Manage::install($plugin);
    	$output->info('插件安装成功！');
    }
}