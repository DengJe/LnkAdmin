<?php
namespace LnkAdmin\command;

use app\admin\model\auth\AdminModel;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use LnkAdmin\service\AppService;
use LnkAdmin\support\Terminal;

class AdminBuildCommand extends Command
{
    protected function configure()
    {
        $this->setName('admin:build')
        	->setDescription('构建admin web');
    }

    protected function execute(Input $input, Output $output)
    {
    	 // 执行构建
         Terminal::execute(root_path('admin-view'),'npm run build');
    }
}