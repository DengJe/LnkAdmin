<?php


if (!function_exists('success')) {
    /**
     * 返回成功方法
     *
     * @param string $msg
     * @param array $data
     * @return \think\response\Json
     */
    function success($msg = '', $data = null)
    {
        $result = [
            'code' => 200,
            'msg' => $msg,
            'data' => $data,
            'error' => 0,
        ];
        if (is_array($msg) || is_object($msg)) {
            $result = [
                'code' => 200,
                'msg' => 'success',
                'data' => $msg,
                'error' => 0
            ];
        }


        return json($result);
    }
}

if (!function_exists('error')) {
    /**
     * 返回失败方法
     *
     * @param string $msg
     * @param array $data
     * @return \think\response\Json
     */
    function error($msg = '', $error_code = 1, $data = null, $status_code = 200)
    {
        $result = [
            'error' => $error_code,
            'msg' => $msg,
            'data' => $data
        ];

        return json($result, $status_code);
    }
}


if (!function_exists('copy_directory')) {
    /**
     * 复制目录
     *
     * @param mixed            $condition
     * @param Throwable|string $exception
     * @param array            ...$parameters
     * @return mixed
     *
     * @throws Throwable
     */
    function copy_directory($source, $destination)
    {
        if (!is_dir($destination)) {
            mkdir($destination, 0755, true);
        }
        $files = scandir($source);
        foreach ($files as $file) {
            if ($file !== '.' && $file !== '..') {
                $sourceFile = $source . '/' . $file;
                $destinationFile = $destination . '/' . $file;
                if (is_dir($sourceFile)) {
                    copy_directory($sourceFile, $destinationFile);
                } else {
                    copy($sourceFile, $destinationFile);
                }
            }
        }
    }
}


if (!function_exists('delete_directory')) {

    function delete_directory($dir)
    {
        if (!file_exists($dir)) {
            return;
        }
        foreach (scandir($dir) as $file) {
            if ($file == '.' || $file == '..') {
                continue;
            }
            $path = $dir . '/' . $file;
            if (is_dir($path)) {
                delete_directory($path);
            } else {
                unlink($path);
            }
        }
        rmdir($dir);
    }
}


if (!function_exists('encrypt_password')) {
    /**
     * 获取加密后的密码
     */
    function encrypt_password($password, $salt)
    {
        return md5(md5($password) . $salt);
    }
}




if (!function_exists('get_plugins')) {
    /**
     * 获取本地插件列表
     */
    function get_plugins()
    {
    }
}


if (!function_exists('get_domain')) {
    /**
     * 获取当前域名
     */
    function get_domain()
    {
        $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
        $url = $protocol . $_SERVER['HTTP_HOST'];
        return $url;
    }
}


if (!function_exists('admin_path')) {
    /**
     * 获取admin目录
     */
    function admin_path($path = "")
    {

        return __DIR__ . DIRECTORY_SEPARATOR . $path;
    }
}



if (!function_exists('list_to_tree')) {
    /**
     * 列表转换成树形
     */
    function list_to_tree($items, $parentId = null, $keyName = 'id', $parentKeyName = 'parent_id',$children='children')
    {
        $tree = [];
        foreach ($items as $k => $item) {
            if ($item[$parentKeyName] == $parentId) {
                $item[$children]  = list_to_tree($items, $item[$keyName]);
                $tree[] = $item;
            }
        }
        return $tree;
    }
}
