<?php
declare (strict_types=1);

namespace LnkAdmin\service;

use LnkAdmin\support\SqlExecute;
use think\facade\Cache;
use think\facade\Env;


class InstallService 
{

        public static function putEnv($databaseEnv)
        {
            if(is_file(root_path().'.env')){
                return false;
            }

            $applyDbEnv = [
                'DB_NAME' => $databaseEnv['database'] ?? '',
                'DB_HOST' => $databaseEnv['hostname'],
                'DB_USER' => $databaseEnv['username'] ?? '',
                'DB_PASS' => $databaseEnv['password'] ?? '',
                'DB_PORT' => $databaseEnv['hostport'] ?? '',
                'DB_PREFIX' => $databaseEnv['prefix'] ?? '',
                'JWT_TTL' => 3600 * 24 * 7
            ];
            $env = new Env();
            $env::load(root_path().'.example.env');
        
            $exampleArr = $env::get();

            $envLine = array_merge($exampleArr, $applyDbEnv);
    
            $content = '';
      
    
            foreach ($envLine as $index => $value) {
    
                    $content .= $index ." = ". $value."\n";
                
            }
    
            if (!empty($content)) {
                $envFilePath = root_path().'.env';
                file_put_contents($envFilePath, $content);
            }
        }

        /**
         * 导入数据库
         */
        public static function execSql($mysqlInfo)
        {
            $sql = new SqlExecute($mysqlInfo);
            $sql->execInstallSql('admin');
            $sql->execDataSql('admin');
        }
}