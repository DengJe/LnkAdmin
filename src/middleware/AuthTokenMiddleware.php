<?php

declare(strict_types=1);

namespace LnkAdmin\middleware;

use dengje\jwt\exception\TokenExpiredException;
use dengje\jwt\exception\TokenBlacklistGracePeriodException;
use dengje\jwt\exception\TokenBlacklistException;
use dengje\jwt\exception\TokenInvalidException;
use dengje\jwt\middleware\BaseMiddleware;
use LnkAdmin\facade\Auth;
use LnkAdmin\exception\LnkException;

class AuthTokenMiddleware 
{
    public function handle($request, \Closure $next, $guard = null)
    {
       
   

        // 验证token
        try {
            $guard = 'admin';
            $user = Auth::guard($guard)->user();
            if (!$user) {
             
                throw new \think\exception\HttpException(401, 'TOKEN无效');
            }
        } catch (TokenExpiredException $e) { // 捕获token过期
            // 尝试刷新token
            try {
                // 重新获取token
                $token = $this->auth->refresh();
                session('header_authorization', $token);        // 将新的 token 存入 session

                // 重新登录，保证这次请求正常进行
                $payload = $this->auth->auth(false);
                Auth::guard($guard)->loginUsingId($payload['uid']->getValue());

                // return $next($request);
                // return $this->setAuthentication($response, $token);
            } catch (TokenBlacklistException $e) {
                throw new \think\exception\HttpException(401, 'TOKEN无效');
            } catch (TokenBlacklistGracePeriodException $e) { // 捕获黑名单宽限期
                throw new \think\exception\HttpException(401, 'TOKEN无效');
            } catch (TokenExpiredException $e) {
                // 续期失败,需要重新登录
                throw new \think\exception\HttpException(401, 'TOKEN无效');
            }
        } catch (TokenBlacklistException $e) {
            throw new \think\exception\HttpException(401, 'TOKEN无效');
        } catch (TokenBlacklistGracePeriodException $e) { // 捕获黑名单宽限期
            throw new \think\exception\HttpException(401, 'TOKEN无效');
        } catch (TokenInvalidException $e) { // token 无效
            throw new \think\exception\HttpException(401, 'TOKEN无效');
        }

        return $next($request);
    }
}
