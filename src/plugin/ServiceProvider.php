<?php
namespace LnkAdmin\plugin;
use LnkAdmin\exception\LnkException;
use think\facade\App;
use think\facade\Route;

abstract class ServiceProvider extends  \think\Service
{

  
    /**
     * 安装插件
     */
    public abstract function install();
   

    /**
     * 卸载插件
     */
    public abstract function uninstall();


    /**
     * 启用插件
     */
    public abstract function enable();

    /**
     * 禁用插件
     */
    public abstract function disable();

    /**
     * 更新插件
     */
    public abstract function update();


   

}
