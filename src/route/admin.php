<?php

use app\admin\controller\Config;
use think\facade\Route;


// 登录
Route::post('admin/user/login','LnkAdmin\controller\Index@login');
 // 初始化配置信息
 Route::get('/admin/config/initConfig','LnkAdmin\controller\Config@initConfig');
 Route::get('/terminal/test','Terminal/test');

Route::group('/install',function(){
    // 
    Route::get('/index','LnkAdmin\controller\Install@index');
    Route::any('/step1','LnkAdmin\controller\Install@step1');
    Route::any('/step2','LnkAdmin\controller\Install@step2');
    Route::any('/step3','LnkAdmin\controller\Install@step3');
    Route::any('/','LnkAdmin\controller\Install@install');
    Route::any('/clear','LnkAdmin\controller\Install@clear');
    

});

Route::group('/admin',function(){
    // 获取用户信息
    Route::get('/user/profile','LnkAdmin\controller\Index@profile');
    // 用户菜单
    Route::get('/user/menu','LnkAdmin\controller\Index@userMenu');
    // 存储列表
    Route::get('/storage/list','LnkAdmin\controller\setting\Storage@list');
    // 存储详情
    Route::get('/storage/detail','LnkAdmin\controller\setting.Storage@detail');
    // 设置存储
    Route::post('/storage/setup','LnkAdmin\controller\setting\Storage@setup');
    // 上传图片
    Route::post('/upload/image','LnkAdmin\controller\Upload@image');
    // 文件组
    Route::resource('/file/group',\LnkAdmin\controller\FileGroup::class);
    // 文件列表
    Route::get('/file/list','LnkAdmin\controller\File@list');
    // 更改管理员密码
    Route::put('/auth/admin/updatePassword','\LnkAdmin\controller\auth\Admin@updatePassword');
    // 管理员
    Route::resource('/auth/admin',\LnkAdmin\controller\auth\Admin::class);
    // 更新角色菜单权限
    Route::post('/auth/role/updateMenuPermission','\LnkAdmin\controller\auth\Role@updateMenuPermission');
    // 更新角色数据权限
    Route::post('/auth/role/updateDataPermission','\LnkAdmin\controller\auth\Role@updateDataPermission');
    // 角色
    Route::resource('/auth/role',\LnkAdmin\controller\auth\Role::class);
    
    // 菜单
    Route::get('/auth/permission/options','LnkAdmin\controller\auth\Permission@tree');
    // 部门
    Route::resource('/auth/dept',\LnkAdmin\controller\auth\Dept::class);
    // 权限
    Route::resource('/auth/permission',\LnkAdmin\controller\auth\Permission::class);
    // 角色列表tree
    Route::get('/auth/roleTree','LnkAdmin\controller\auth\Role@tree');
    // 获取系统配置
    Route::get('/system/detail','LnkAdmin\controller\setting\System@detail');
    // 更新系统配置
    Route::post('/system/update','LnkAdmin\controller\setting\System@update');
    // 更新菜单权限
    Route::put('/role/updateRolePermissions','LnkAdmin\controller\auth\Role@updateRolePermissions');
    // 字典类型
    Route::resource('/dict/type',\LnkAdmin\controller\dict\DictType::class);
    // 字典数据
    Route::resource('/dict/data',\LnkAdmin\controller\dict\DictData::class);
    // 应用列表
    Route::get('/plugin/list','LnkAdmin\controller\Plugin@list');
    // 安装/更新应用
    Route::post('/plugin/install','LnkAdmin\controller\Plugin@install');
    // 启用应用
    Route::post('/plugin/enable','LnkAdmin\controller\Plugin@enable');
    // 禁用应用
    Route::post('/plugin/disable','LnkAdmin\controller\Plugin@disable');
    
    
})->middleware('auth','admin');




