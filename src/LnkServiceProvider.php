<?php

namespace LnkAdmin;


use think\Service;
use LnkAdmin\command\AdminInstallCommand;
use LnkAdmin\command\PluginCreateCommand;
use LnkAdmin\command\AdminBuildCommand;
use LnkAdmin\command\AdminPublishCommand;
use LnkAdmin\command\PluginInstallCommand;
use LnkAdmin\library\auth\Auth;
use LnkAdmin\library\plugin\Plugin;
use LnkAdmin\middleware\AuthTokenMiddleware;
use LnkAdmin\middleware\Cors;
use LnkAdmin\plugin\Manage;
use plugin\test\Service as TestService;
use think\App;
use think\facade\Middleware;
use think\facade\Route;
use think\facade\View;

class LnkServiceProvider extends Service
{

    public $bind = [
        'auth' => Auth::class,       // 用户认证服务
        // 'redis' => Redis::class,        // 注册 redis 服务
        // 'storage' => Storage::class,  // 文件上传服务

    ];
   
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }


    public function register()
    {
        
     
        // 导入admin路由
        (new Admin())->loadAdminRoute();
       
        

    
        // 注册插件服务
        $this->registerService();
        
      

        // 注册中间件
        $this->registerMiddleWares();
        $this->commands([
            // MigrateRollback::class,
            // MigrateRun::class,
            // SeedRun::class,
            AdminPublishCommand::class,
            AdminInstallCommand::class,
            AdminBuildCommand::class,
            PluginCreateCommand::class,
            PluginInstallCommand::class,
            // PluginComposerCommand::class
        ]);
    }


    /**
     * 注册中间件
     */
    protected function registerMiddleWares()
    {
        $middleware = $this->app->config->get('middleware');

        $middleware['alias']['auth'] = [
            AuthTokenMiddleware::class,
        ];
        $middleware['alias']['cors'] = [
            Cors::class,
        ];

        $this->app->config->set($middleware, 'middleware');
    }



    /**
     * 注册插件服务
     */
    protected function registerService()
    {
        foreach(Admin::getServices() as $service){
            $this->app->register($service);
        }
    }


    /**
     * 注册插件路由
     */
    // protected function registerPluginRoute()
    // {
    //     $this->app->request->setRoot('/' . 'plugin');
    //     foreach(Admin::getPluginRoutes() as $path){
    //         Route::group('/plugin',function()use($path){
    //             $this->loadRoutesFrom($path);
    //             // include $path;
    //         });
            
    //     }
       
    // }

}