import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'



// https://vitejs.dev/config/
export default defineConfig({
  base: './',
  envPrefix: 'LNK_',
  plugins: [vue()],
  resolve: {
    // 配置路径别名
    alias: {
      '@': path.resolve(__dirname, './src'),
    },
  },
  build:{
    outDir:'../public/super'
  }

})
