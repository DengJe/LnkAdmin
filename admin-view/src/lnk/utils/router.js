
export function getRouter(permission = []) {
    const routers = []
    const menu = []
    menuRecursion(permission, routers)
    menuRecursion2(permission, menu)
    return { routers, menu };
}

// 递归设置路由信息
function menuRecursion(permission = [], routers = []) {
    permission.forEach((item, index) => {
        const route = getRealRoute(item);
        routers.push(route)
        if (item.children.length !== 0) {
            const children = item.children;
            // 递归实现
            menuRecursion(children, routers[index].children)
        }
    })
}

function menuRecursion2(permission = [], routers = []) {
    permission.forEach((item, index) => {
        const route = getRealRoute(item);
        if(route.meta.status == 'show'){
            routers.push(route)
        }
        
        if (item.children.length !== 0 && route.meta.status == 'show') {
            const children = item.children;
            // 递归实现
            menuRecursion2(children, routers[index].children)
        }
    })
}

const pluginModules = import.meta.glob('../../app/*/views/**/*.vue')
const adminModules = import.meta.glob('../../lnk/admin/views/**/*.vue')
const modules = { ...pluginModules, ...adminModules }
// console.log(modules);
/**
 * 获取路由标准
 */
function getRealRoute(item) {

    // 路由基本格式
    return {
        // 路由的路径
        path: item.route,
        // 路由名
        name: item.permission_mark,
        // a:`@/app${item.component}.vue`,
        // 路由所在组件
        // component: item.component ? resolve => import(/* @vite-ignore */`/app${item.component}.vue`):'',
        component: modules[`../../app/${item.component}.vue`] ? modules[`../../app/${item.component}.vue`] : modules[`../${item.component}.vue`],
        meta: {
            id: item.id,
            icon: item.icon,
            path: item.route,
            title: item.permission_name,
            type: item.type,
            redirect: item.redirect,
            status: item.status,
        },
        // 路由的子路由
        children: []
    }
}