
import http from "@/lnk/utils/request"


const CRUD = (path) => {
  return {
    // 新增
    create: async function(data, config={}){
        return await http.post(path, data, config);
    },
    // 删除
    delete: async function(data, config={}){
        return await http.delete(`${path}/${data.id}`, data, config);
    },
    // 更新
    update: async function(data, config={}){
        return await http.put(`${path}/${data.id}`, data, config);
    },
    // 列表
    list: async function(params, config={}){
        return await http.get(path, params, config);
    },
    // 详情
    read:async function(params, config={}){
        return await http.get(`${path}/${params.id}`, params, config);
    },
  };
};

export default CRUD;