import {
    ElMessage,
    ElMessageBox,
    ElNotification,
    ElLoading,
} from 'element-plus'


export class Feedback {
    static instance
    static getInstance() {
        return this.instance ?? (this.instance = new Feedback())
    }
    // 消息提示
    msg(msg) {
        ElMessage.info(msg)
    }
    // 错误消息
    msgError(msg) {
        ElMessage.error(msg)
    }
    // 成功消息
    msgSuccess(msg) {
        ElMessage.success(msg)
    }
    // 警告消息
    msgWarning(msg) {
        ElMessage.warning(msg)
    }
    // 弹出提示
    alert(msg) {
        ElMessageBox.alert(msg, '系统提示')
    }
    // 错误提示
    alertError(msg) {
        ElMessageBox.alert(msg, '系统提示', { type: 'error' })
    }
    // 成功提示
    alertSuccess(msg) {
        ElMessageBox.alert(msg, '系统提示', { type: 'success' })
    }
    // 警告提示
    alertWarning(msg) {
        ElMessageBox.alert(msg, '系统提示', { type: 'warning' })
    }
    // 通知提示
    notify(msg) {
        ElNotification.info(msg)
    }
    // 错误通知
    notifyError(msg) {
        ElNotification.error(msg)
    }
    // 成功通知
    notifySuccess(msg) {
        ElNotification.success(msg)
    }
    // 警告通知
    notifyWarning(msg) {
        ElNotification.warning(msg)
    }
    // 确认窗体
    confirm(msg) {
        return ElMessageBox.confirm(msg, '温馨提示', {
            confirmButtonText: '确定',
            cancelButtonText: '取消',
            type: 'warning'
        })
    }
    // 提交内容
    prompt(content, title, options={}) {
        return ElMessageBox.prompt(content, title, {
            confirmButtonText: '确定',
            cancelButtonText: '取消',
            ...options
        })
    }
    // 打开全局loading
    loading(msg) {
        this.loadingInstance = ElLoading.service({
            lock: true,
            text: msg
        })
    }
    // 关闭全局loading
    closeLoading() {
        this.loadingInstance?.close()
    }
}

const feedback = Feedback.getInstance()

export default feedback