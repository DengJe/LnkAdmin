
import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router';
import testRouter from './test'
import tools from '@/lnk/utils/tools'
import systemRoute from './test.js';
import userStore from '@/lnk/admin/store/user'
// 系统静态路由
const routes = [
  { name: 'Login', path: '/login', component: () => import('@/lnk/admin/views/login/index.vue'), },
  { name: 'layout', path: '/', component: () => import('@/lnk/layout/index.vue'), },
  { name: 'iframe', path: '/iframe', component: () => import('@/lnk/layout/index.vue'), },
  // { name:'profile',path: '/profile', component: () => import('@/app/admin/views/user/profile.vue'), },



];

routes.push(...systemRoute);



routes.push(testRouter)
const router = createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHashHistory(),
  routes, // `routes: routes` 的缩写
})




// 路由前置
router.beforeEach(async (to, from, next) => {


  // 判断是否有登录
  let token = tools.storage.get('token')
  if(token){
    if (userStore.state.routes.length == 0) {
      const data = await userStore.dispatch('profile')
      data && next({ ...to, replace: true })
    }else{
      next()
    }
    
  }else{
   
    if (to.name !== 'Login') {
      // 将用户重定向到登录页面
      router.push('/login')
      return;
    }else{
      next();
    }
  }
  
  
})


export default router