import Layout from '@/lnk/layout/index.vue';
import appStore from '@/lnk/admin/store/app'
console.log(appStore)
const routes = [
  
  {
    name: 'index',
    path: '/index',
    redirect: { name: 'Index' },
    component: Layout,
    meta: { title: '首页', icon: 'setting-outlined' },
    children: [
      { name:'profile',path: '/profile', component: () => import('@/lnk/admin/views/user/profile.vue'),meta:{title:'用户资料'} },
      { name:'dashboard',path: '/dashboard', component: () => import('@/lnk/admin/views/dashboard/index.vue'),meta:{title:'Dashboard'} },
      
    ]
  }
]
export default routes;
