// 设置请求基础配置
let baseURL;
switch (import.meta.env.MODE) {
  case 'development':
    baseURL = import.meta.env.LNK_DEV_BASE_URL;
    break;
  case 'development-mock':
    baseURL = import.meta.env.LNK_DEV_MOCK_URL;
    break;
  default:
    baseURL = import.meta.env.LNK_BASE_URL;
}

const version = import.meta.env.LNK_VERSION;

console.log(
  `LnkAdmin ${version}  https://www.lnkadmin.com/`,
);
const config = {
    baseURL,
    version,
    //是否加密localStorage, 为空不加密，可填写AES(模式ECB,移位Pkcs7)加密
	  LS_ENCRYPTION: '',

	  //localStorageAES加密秘钥，位数建议填写8的倍数
	  LS_ENCRYPTION_key: '8INB8K8BC08UVUN8',
}
export default config;
