import tools from './utils/tools'
import http from './utils/request'

import appStore from '@/lnk/admin/store/app'

// lnkAdmin组件
import laIconPicker from '@/lnk/components/la-icon-picker/index.vue'
import laPagination from '@/lnk/components/la-pagination/index.vue';
import laUploader from '@/lnk/components/la-uploader/index.vue'
import laEditor from '@/lnk/components/la-editor/index.vue'


export default {
	async install(app) {
        const lnk = {
            tools,
            http,
        };
        
        // 加载初始配置
        await appStore.dispatch('getInitConfig')
      
        app.config.globalProperties.$lnk = lnk;
        app.component('laIconPicker',laIconPicker);
        app.component('laPagination',laPagination);
        app.component('laUploader',laUploader);
        app.component('laEditor',laEditor);

        


        
    
       
    }
}