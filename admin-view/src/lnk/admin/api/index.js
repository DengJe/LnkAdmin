
import http from "@/lnk/utils/request"
import crud from "@/lnk/utils/crud"

export default {

	config: {
		getInitConfig: async function () {
			return await http.get('admin/config/initConfig')
		},
		...crud('/admin/config'),
		

	},

	role: {
		...crud('/admin/auth/role'),
		roleTree: async function (params) {
			return await http.get('admin/auth/roleTree', params);
		},
		updateMenuPermission: async function (data) {
			return await http.post('admin/auth/role/updateMenuPermission', data);
		},
		updateDataPermission: async function (data) {
			return await http.post('admin/auth/role/updateDataPermission', data);
		}
	},
	admin: {
		login: async function (data) {
			return await http.post('admin/user/login', data);
		},
		profile: async function (data) {
			return await http.get('admin/user/profile', data);
		},
		...crud('/admin/auth/admin'),
		updatePassword: async function (data) {
			return await http.put('admin/auth/admin/updatePassword', data);
		}


	},
	menu: {
		...crud('/admin/auth/permission'),
		optionsTree: async function () {
			return await http.get('/admin/auth/permission/options');
		}
	},
	dept:{
		...crud('/admin/auth/dept'),
	},
	dictType:{
		...crud('/admin/dict/type'),
	},
	dictData:{
		...crud('/admin/dict/data'),
	},
	plugin: {
		list: async function () {
			return await http.get('/admin/plugin/list');
		},
		install: async function (data) {
			return await http.post('/admin/plugin/install',data);
		},
		enable: async function (data) {
			return await http.post('/admin/plugin/enable',data);
		},
		disable: async function (data) {
			return await http.post('/admin/plugin/disable',data);
		},
	},
	storage: {
		list: async function () {
			return await http.get('/admin/storage/list');
		},
		detail: async function (data) {
			return await http.get('/admin/storage/detail', data);
		},
		setup: async function (data) {
			return await http.post('/admin/storage/setup', data);
		},

	},
	system:{
		detail: async function (data){
			return await http.get('/admin/system/detail');
		},
		update: async function (data){
			return await http.post('/admin/system/update',data);
		},
	},
	file: {
		list: async function (data) {
			return await http.get('/admin/file/list', data);
		},

	},
	fileGroup: {
		...crud('/admin/file/group')
	},
	upload: {
		// 图片上传
		image: async function (data) {
			return await http.post('/admin/upload/image', data, { headers: { 'Content-Type': 'multipart/form-data' } })
		}
	}


}
