/**
 * @description 用户信息、登录、退出登录
 */
import { createStore } from 'vuex';
import api from '@/lnk/admin/api';
import router from "@/lnk/router";

export default createStore({
  namespaced: true,
  state: () => ({
    conifg:{}

  }),
  getters: {
    getRoutes(state) {
      return [];
    }
  },
  actions: {

    /**
     * 获取初始化配置
     * @returns 
     */
    async getInitConfig() {
      let {data} = await api.config.getInitConfig()
      this.state.config = data
       // 设置控制台
      this.dispatch('setDashboard');
    },

    /**
     * 获取图片访问地址
     * @param {*} uri 
     * @returns 
     */
    getImageUrl(context,uri) {
        return uri ? `${this.state.config.oss_domain}${uri}` : ''
    },

    /**
     * 修改控制台路视图
     */
    async setDashboard(){
      const modules = import.meta.glob('@/lnk/*/views/**/*.vue')
      router.addRoute('layout',{'path':'/dashboard',name:'dashboard',component:  modules[`/src/app/${this.state.config.dashboard_component}.vue`]})
    }

    
  },
})
