/**
 * @description 用户信息、登录、退出登录
 */
import { createStore } from 'vuex';
import api from '@/lnk/admin/api';
import { getRouter } from '@/lnk/utils/router'
import router from '@/lnk/router';
import appStore from "@/lnk/admin/store/app";

export default createStore({
  namespaced: true,
  state: () => ({
    menu: [], // 菜单
    permissions: [], // 菜单权限
    routes: [], // 路由
    userInfo: {},// 用户信息

  }),
  getters: {
    getRoutes(state) {
      return state.routes
    }
  },
  actions: {
    /**
     * @description 登录
     * @param {*} formData
     */
    async login(formData) {
      return await admin.account.login(formData);
    },

    /**
     * @description 设置token
     * @param {*} token
     */
    setToken(token = '') {
      this.token = token;
      if (token == '') {
        this.isLogin = false;
        $storage.remove('token');
      } else {
        this.isLogin = true;
        $storage.set('token', token);
      }
    },

    /**
     * @description 获取用户信息
     */
    async profile(context) {
      const { error, data } = await api.admin.profile();
      
      // 用户信息
      context.state.userInfo = data.user
      // 设置路由
      await context.dispatch('setRoute', data.permission)
      // 设置菜单
      await context.dispatch('setMenu', data.permission)

      return data;
    },
    setRoute(context,payload){
      const {routers,menu} = getRouter(payload)
      console.log(9999,menu)
      context.state.routes = routers;
      const systemMenu = []
      routers.unshift(...systemMenu)
      routers.map(item => router.addRoute('layout', item))
      // 放在这里防止动态路由没有加载完出现刷新404
      if (!router.hasRoute("NotFound")) {
        router.addRoute({
          path: "/:pathMatch(.*)*",
          name: 'NotFound',
          meta: {
            title: 'Page not found',
          },
          component: () => import('@/lnk/admin/views/common/404.vue')
        })
      }
    },
    setMenu(context,payload) {
   
       context.state.menu = getRouter(payload).menu
       
     
    },

    /**
     * @description 获取用户菜单权限规则
     */
    async getRules() {
      // const { error, data } = await admin.account.rules();
      // if (error === 0) {
      //   this.menuRules = data.menu; // 设置菜单规则
      //   this.pageRules = data.permission; // 设置页面权限
      //   this.apiRules = data.permission; // 设置功能权限
      //   return data;
      // }
    },

    /**
     * @description 退出登录
     * @param force
     */
    async logout(force = false) {
      let that = this;
      if (!force) {
        await admin.account.logout();
      }
      setTimeout(function () {
        that.info = {};
        that.setToken();
        that.setRules({ menu: [], page: [], api: [] });
        app().setTaskbar({ list: [], history: [] });
        window.location = location.pathname;
      }, 0);
    },

    /**
     * @description 设置菜单规则
     * @param payload
     */
    setRules(payload) {
      this.menuRules = payload.menu; // 设置菜单规则
      this.pageRules = payload.permission; // 设置页面权限
      this.apiRules = payload.permission; // 设置功能权限
    },
  },
})
