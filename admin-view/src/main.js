import { createApp } from 'vue'

import App from './App.vue'
import router from '@/lnk/router'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import lnk from '@/lnk/index.js';
import './style.css'
import '@/lnk/styles/public.css'
import 'element-plus/theme-chalk/dark/css-vars.css'



const app = createApp(App);
// lnkadmin组件
app.use(lnk)
//整个应用支持路由。
app.use(router)
// ui
app.config.globalProperties.$icons = []
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.config.globalProperties.$icons.push(key)
    app.component(key, component)
  }
app.use(ElementPlus)






app.mount('#app')

